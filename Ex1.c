#include <stdio.h>
#include <stdlib.h>
#include <math.h>


float num1, num2;
int dividir, soma;

int main(void)
{
    printf("Informe um valor float: ");
    scanf("%f", &num1);

    printf("Informe outro valor float: ");
    scanf("%f", &num2);


    dividir = num1 / num2;
    printf("%.2lf / %.2lf = %i\n", num1, num2, dividir);


    soma = (num1 + num2) + 1;
    printf("%.2lf + %.2lf = %i\n", num1, num2, soma);


    system("pause");
    return 0;
}
