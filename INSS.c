
#include <stdio.h>
#include <math.h>
#include <stdlib.h>


int main(void)
{
    // Entrada de dados e Armazenamento
    float SM, SB, SL, INSS;

    printf("Informe o salario minimo: R$");
    scanf("%f", &SM);

    printf("Informe o valor do seu salario bruto: R$");
    scanf("%f", &SB);

    // Opera��es de decis�o
    if (SB <= SM) // Salario menor que 1 salario minimo
    {
        INSS = SB * 0.08;
        scanf(&INSS);
        SL = SB - INSS;
        printf("Seu salario final depois do desconto de 8%% do INSS: R$ %.2lf\n", SL);

    }

    else // Sal�rio maior que 1 sal�rio m�nimo
    {
        INSS = SB * 0.1;
        if(INSS >= SM) // Caso o valor do INSS seja maior ou igual a um salario minimo
            {
                SL = SB - SM;
                printf("Seu salario final depois do desconto foi de: R$ %.2lf\n", SL);


            }

            else // Caso o INSS n�o seja maior que um sal�rio m�nimo
            {
                SL = SB - (SB * 0.1);
                printf("Seu salario final depois do desconto de 10%% do INSS: R$ %.2lf\n", SL);
            }


    }

    system("pause");
    return 0;
}
