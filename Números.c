#include <stdio.h>
#include <math.h>
#include <stdlib.h>


int main(void)
{
    int opcao, a, b, c;

    //Coleta de Dados
    printf("Informe o primeiro numero: ");
    scanf("%d", &a);

    printf("Informe o segundo numero: ");
    scanf("%d", &b);

    printf("Informe o terceiro numero: ");
    scanf("%d", &c);

    printf("-------- Bem Vindo! --------\n");
    printf("1- Numeros em ordem crescente\n2- Numeros em ordem decrescente\n3- Numeros que sao multiplos de 2\nDigite a opcao desejada: ");
    scanf("%d", &opcao);


    //Processamento de dados

    if (opcao != 1 && opcao != 2 && opcao != 3)
        {
        printf("Opcao invalida, feche o programa e abra novamente!\n");
    }

    else
        {
            switch(opcao)
    //Caso de numeros crescentes
    {
        case 1:
            if (a>b && a>c && b>c)
                {
                    printf("Os numeros em ordem crescente sao: %i, %i, %i\n", c, b, a);
                }
            else if (a>b && a>c && c>b)
                {
                    printf("Os numeros em ordem crescente sao: %i, %i, %i\n", b, c, a);
                }
            else if (b>a && b>c && a>c)
                {
                    printf("Os numeros em ordem crescente sao: %i, %i, %i\n", c, a, b);
                }
            else if (b>a && b>c && c>a)
                {
                    printf("Os numeros em ordem crescente sao: %i, %i, %i\n", a, c, b);
                }

            else if (c>b && c>a && b>a)
                {
                    printf("Os numeros em ordem crescente sao: %i, %i, %i\n", a, b, c);
                }
            else if (c>b && c>a && a>b)
                {
                    printf("Os numeros em ordem crescente sao: %i, %i, %i\n", b, a, c);
                }

            break;

    //Caso numeros decrescentes
        case 2:
            if (a<b && a<c && b<c)
                {
                    printf("Os numeros em ordem decrescente sao: %i, %i, %i\n", c, b, a);
                }
            else if (a<b && a<c && c<b)
                {
                    printf("Os numeros em ordem decrescente sao: %i, %i, %i\n", b, c, a);
                }
            else if (b<a && b<c && a<c)
                {
                    printf("Os numeros em ordem decrescente sao: %i, %i, %i\n", c, a, b);
                }
            else if (b<a && b<c && c<a)
                {
                    printf("Os numeros em ordem decrescente sao: %i, %i, %i\n", a, c, b);
                }

            else if (c<b && c<a && b<a)
                {
                    printf("Os numeros em ordem decrescente sao: %i, %i, %i\n", a, b, c);
                }
            else if (c<b && c<a && a<b)
                {
                    printf("Os numeros em ordem decrescente sao: %i, %i, %i\n", a, b, c);
                }

            break;

    // Caso numeros multiplos de 2
        case 3:

            // Tres sao multiplos
            if ((a % 2 == 0) && (b % 2 == 0) && (c % 2 == 0))
                {
                    printf("%i, %i e %i sao multiplos de 2\n", a, b, c);
                }

            // Dois numeros sao multiplos
            else if ((a % 2 == 0) && (b % 2 == 0) & (c % 2 != 0))
                {
                    printf("Apenas %i e %i sao multiplos de 2\n", a, b);
                }
            else if ((a % 2 == 0) && (c % 2 == 0) & (b % 2 != 0))
                {
                    printf("Apenas %i e %i sao multiplos de 2\n", a, c);
                }

            else if ((b % 2 == 0) && (c% 2 == 0) & (a % 2 != 0))
                {
                    printf("Apenas %i e %i sao multiplos de 2\n", b, c);
                }

           // Apenas 1 numero eh multiplo
            else if ((a % 2 == 0) && (b % 2 != 0) & (c % 2 != 0))
                {
                    printf("Apenas %i eh multiplo de 2\n", a);
                }

            else if ((b % 2 == 0) && (c % 2 != 0) & (a % 2 != 0))
                {
                    printf("Apenas %i eh multiplo de 2\n", a);
                }

            else if ((c % 2 == 0) && (a % 2 != 0) & (b % 2 != 0))
                {
                    printf("Apenas %i eh multiplo de 2\n", a);
                }


            break;

    }

        }


    system("pause");
    return 0;


}
