#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float KMS, combustivel, valorCombustivel, Kml,gasto;

int main(void)
{

    printf("Informe os KMS percorridos: ");
    scanf("%f", &KMS);

    printf("Informe o combustivel consumido (em litros): ");
    scanf("%f", &combustivel);

    printf("Informe o valor do litro da gasolina: R$");
    scanf("%f", &valorCombustivel);

    Kml = KMS / combustivel;

    gasto = (valorCombustivel * combustivel) / KMS;

    printf("O automovel fez %.2lf km por litro de combustivel\n", Kml);
    printf("O gasto em reais foi de R$%.2lf\n", gasto);



    system("pause");
    return 0;


}
