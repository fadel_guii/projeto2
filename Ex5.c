#include <stdio.h>
#include <stdlib.h>
#include <math.h>


float VV, VB, VN, VT, pVV, pVB, pVN;


int main(void)
{
    printf("Informe o numero de votos validos: ");
    scanf("%i", &VV);

    printf("Informe o numero de votos em branco: ");
    scanf("%i", &VB);

    printf("Informe o numero de votos nulos: ");
    scanf("%i", &VN);

    VT = VV + VB + VN;

    pVV = (VV / VT) * 100;

    pVB = (VB / VT) * 100;

    pVN = (VN / VT) * 100;

    printf("Numero de votos validos: %.2lf%%\n", pVV);
    printf("Numero de votos em branco: %.2lf%%\n", pVB);
    printf("Numero de votos nulos: %.2lf%%\n", pVN);


    system("pause");
    return 0;
}
