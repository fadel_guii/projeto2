#include <stdio.h>
#include <math.h>
#include <stdlib.h>

float SB, INSS, IR, SL;

int main(void)
{

    printf("Digite o Salario Bruto: ");
    scanf("%f", &SB);

    printf("Digite o percentual de INSS: ");
    scanf("%f", &INSS);


    printf("Digite o valor percentual de IR: ");
    scanf("%f", &IR);


    SL = SB - (((INSS/100) * SB) + ((IR/100) * SB));


    printf("O salario liquido: R$ %.2lf", SL);


    system("pause");
    return 0;

}

