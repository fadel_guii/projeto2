#include <stdio.h>
#include <math.h>
#include <stdlib.h>

float A, B, At, Bt;

int main(void){

    printf("Digite o valor de A: ");
    scanf("%f", &A);

    printf("Digite o valor de B: ");
    scanf("%f", &B);

    At = B;
    Bt = A;

    printf("Valor antes da troca: A = %.2lf e B = %.2lf\n", A, B);
    printf("Valor depois da troca: A = %.2lf e B = %.2lf\n", At, Bt);

    system("pause");
    return 0;



}

