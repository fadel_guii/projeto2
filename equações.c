#include <stdio.h>
#include <math.h>
#include <stdlib.h>


float x, y, op1, op2, op3, op4, op5, Qdivis;



int main(void)

{

    printf("Digite um valor para X: ");
    scanf("%f", &x);

    printf("Digite um valor para Y: ");
    scanf("%f", &y);

    //Opera��es

    Qdivis = x/y;

    op1 = ((x + y)/y) * pow(x, 2);

    op2 = (x+y)/(x - y);

    op3 = ((pow(x, 2)) + (pow(y, 3)))/2;

    op4 = pow(x, 3)/pow(y, 2);

    op5 = x/(y * Qdivis);


    // Resultados
    printf("O primeiro valor: %.2lf\n", op1);

    printf("O segundo valor: %.2lf\n", op2);

    printf("O terceiro valor: %.2lf\n", op3);

    printf("O quarto valor: %.2lf\n", op4);

    printf("O quinto valor: %.2lf\n", op5);


    system("pause");
    return 0;

}
