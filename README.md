# Bem vindo ao meu repositório

Prazer, meu nome é Guilherme Fadel e eu sou aluno do curso de 
Engenharia de Computação da UTFPR-PB.

Esse é meu repositório de programas feitos durante o curso.

Ele tem o objetivo de:
* Mostrar meu percurso durante o curso
* Servir de lembrança para possíveis coisas que posso esquecer


Sinta-se livre para dar uma olhada!!



