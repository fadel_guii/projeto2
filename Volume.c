#include <stdio.h>
#include <math.h>
#include <stdlib.h>


float Altura, Comprimento, Largura, Volume;

int main(void){

    printf("Informe a Altura: ");
    scanf("%f", &Altura);

    printf("Informe o Comprimento: ");
    scanf("%f", &Comprimento);

    printf("Informe a Largura: ");
    scanf("%f", &Largura);

    Volume = Altura * Comprimento * Largura;
    printf("O volume da figura: %.2lf", Volume);


    system("pause");
    return 0;

}
