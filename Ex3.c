#include <stdio.h>
#include <stdlib.h>
#include <math.h>


float real, dolar, dividir;

int main(void)
{

    printf("Digite o valor em reais que deseja ser convertido: R$");
    scanf("%f", &real);

    printf("Digite o valor do dolar atual: R$");
    scanf("%f", &dolar);

    dividir = real / dolar;

    printf("R$%.2lf equivalem a U$%.2lf", real, dividir);


    system("pause");
    return 0;


}
